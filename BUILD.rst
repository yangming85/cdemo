Instructions to create releases
===============================

1. Test that everything works with Python 2 and Python 3::

     python2 -m robot login_tests.robot
     python3 -m robot login_tests.robot

2. If needed, move regenerated log and report to
   https://bitbucket.org/robotframework/robotframework.bitbucket.org/src/master/CDemo/
   to make them visible online.

3. Generate a new download package::

     ./package.py

4. Upload the download package to https://bitbucket.org/robotframework/cdemo/downloads
